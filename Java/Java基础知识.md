# java 基础知识

* Java基本数据类型及各自所占的字节数

| 类型 | 字节 |
| :-- | :-- |
| byte | 1个字节 |
| short | 2个字节 |
| int | 4个字节 |
| long | 8个字节 |
| float | 2个字节 |
| double | 4个字节 |
| chart | 2个字节 |
| boolean | 视具体编译环境而定 |

* java中==和equals和hashCode的区别

两个基本数据类型使用`==`，比较的是两个变量值；两个对象使用`==`，比较的是两个对象的内存地址

使用类继承自Object基类，Object类定义了`equals()`方法和`hasCode()`方法，前者的实现调用`==`进行比较，开发者通过重写这两个方法实现自定义比较的逻辑。

重写`equals()`方法，必须重写`hasCode()`方法，两个数值或对象通过equal比较返回true，那么这两个数值或对象的hasCode值一样；反之，两个数值或对象的hasCode值一样，两个数值或对象的equal不一定为true

> [Java中==和equals的区别，equals和hashCode的区别](http://blog.csdn.net/tiantiandjava/article/details/46988461)

* int与integer的区别

int，是一种基本的数据类型，存储的是一个数值，默认值0；integer是一个int的封装类，实例化一个内存地址赋值给一个变量，才可以使用，默认值null

* 探探对java多态的理解

多态是面向对象编程思想的三个基本特征之一，指的是一种动态绑定，在执行期间判断引用对象的实际类型，调用其对应的属性或方法。

作用：消除类型之间的耦合关系

> [浅谈对java多态的理解](http://blog.csdn.net/jerryburning/article/details/45066787)

* String、StringBuffer、StringBuilder区别

String对象一定被创建，它是不可更改的，编程中看到的同一变量名的String对象，重新赋值的过程实际也是重新创建对象的过程，同时JVM回收了“旧对象”，重复的创建和回收，造成String执行缓慢

StringBuilder和StringBuffer对变量重复赋值或改变保留值，不用重复创建和回收对象，执行速度相比会快一些（除一些特殊情况）

StringBuffer的方法用`sychronized`关键字修饰，适合多线程环境下使用，是线程安全的

StringBuilder的方法没有`sychronized`关键字修饰，不保证同步操作，适合单线程环境下使用，速度会比StringBuffer快

> [Java中的String，StringBuilder，StringBuffer三者的区别](https://www.cnblogs.com/su-feng/p/6659064.html)

* 什么是内部类？内部类的作用

内部类，指的是定义在一个类里面的类，分为静态内部类（static修饰的类）、非静态内部类（也叫成员内部类）、方法内部类和匿名内部类，包装内部类的类称为外部类

作用：
内部类提供更好的封装，把内部类隐藏在外部类里面，不允许同一包名的其它类访问该类

保护内部类的隐私数据

* 抽象类和接口区别

抽象类和接口不能直接实例化，抽象类的变量指向实现抽象方法的子类对象，接口的变量指向接口的实现类对象。

抽象类是单一继承，接口可以有多个实现

接口定义的变量只能是公共的静态的常量，抽象类的变量是普通的变量

接口定义的方法只能是公共的抽象的方法，抽象类的方法可以是抽象的方法或非抽象的方法

抽象方法只能被声明，不能实现，接口是设计的结果，抽象类是重构的结果

抽象方法要被实现，所以不能是静态的，也不能是私有的

> [接口和抽象类有什么区别](https://www.cnblogs.com/yongjiapei/p/5494894.html)

* 抽象类的意义

抽象类的意义，要依赖抽象，不要依赖具体类，根据多态的特性，在执行期间判断引用对象的类型，调用对应的属性和方法

* 抽象类与接口的应用场景

在一个继承关系的类中，将同一行为的不同表现提取为抽象的方法，将父类声明为抽象类，应用的场景有：BaseActivity、BaseFragment、BaseAdapter、ViewHolder等基类中

接口应用的场景有：BaseAdapter子类监听item单击事件

* 抽象类是否可以没有方法和属性？

抽象类可以没有方法和属性，使用abstract关键字修饰

* 接口的意义

接口的意义，针对接口编程，不针对实现编程，好处是可以有多个实现类，解耦程序，方便修改、维护。

* 泛型中extends和super的区别

泛型类表示以T为上界的一组子类，适合读取数据，不建议调用写入数据的方法，否则编译器会报错；泛型表示以T为下界的一组超类，适合调用写入数据的方法，不适合读取数据。

> [Android开发之深入理解泛型extends和super的区别](http://teachcourse.cn/2577.html)

* 父类的静态方法能否被子类重写

父类的静态方法可以被子类继承，但不能被重写（即使子类重写了父类的静态方法，最终调用的仍然是父类的静态方法）

原因：`static`修饰的方法，在代码编译时分配了一块内存地址，该内存地址只当程序退出时才会被回收，调用该方法的指针都指向该地址

> [子类能否重写父类的静态方法](http://blog.csdn.net/kdc18333608478/article/details/52040914)

* 进程和线程的区别

进程是分配资源（CPU、内存）的最小单位，线程是程序执行的最小单位

一个进程可以包含多个线程，线程共享整个进程中的资源（CPU、内存），一个进程至少包含一个线程

线程执行开销小，但不利于资源的管理和保护；而进程正相反

线程之间的通信更方便，同一进程下的线程共享全局变量、静态变量等数据；进程间的通信要以通信的方式（IPC）进行

> [一道面试题：说说进程和线程的区别](https://www.cnblogs.com/zhehan54/p/6130030.html)
> 
> [进程和线程的区别](https://www.cnblogs.com/lgk8023/p/6430592.html)
> 
> [进程和线程的区别和联系](http://blog.csdn.net/chen_geng/article/details/51613445)

* 序列化的方式

常用的序列化的方式有：实现Serializable接口、实现Externalizable接口、实现Parcelable接口

另外两种不常用的方式：gson、Google protobuf

> [Java序列化的几种方式](http://blog.csdn.net/endlu/article/details/51178143)

* Serializable 和Parcelable 的区别

Serializable是Java提供的序列化的方式，使用起来简单但是开销大，序列化和反序列化需要进行大量的I/O操作；

Parcelable是android API增加的序列化的方式，缺点是使用起来稍微麻烦点，但是效率较高。

* 静态属性和静态方法是否可以被继承？是否可以被重写？以及原因？

静态属性和静态方法可以被子类继承，但不可以被重写（即使被子类重写，最终调用的仍然是父类的静态属性和静态方法）

原因：`static`修饰的属性和方法，在代码编译期就分配了独立的内存地址，即使子类重写了父类的静态方法，变量指向的还是编译器已分配的内存地址。

* 静态内部类的设计意图

静态内部类实现的效果和独立声明一个类的效果一样，不同之处是静态内部类封装在一个类里

将只想给外部类使用的类声明成静态内部类，可以实现更好的隐藏，不允许除外部类以外的类使用

同时，静态内部类可以通过外部类名区别，可以在不同的类中声明名字一样的静态内部类，比如：Builder

* 成员内部类、静态内部类、局部内部类和匿名内部类的理解，以及项目中的应用

成员内部类，作为外部类的一个成员，可以直接访问外部类的所有属性和方法（包括private属性和方法），成员内部类不允许声明static的变量和方法

应用：一个Activity中，可以声明一个ViewPager的成员内部类，直接使用外部类的数据集

静态内部类，没持有一个指向外部类的引用，不可以使用外部类非static的属性和方法

应用：在使用Builder设计模式的时候，经常需要将内部类设计成static

局部内部类，嵌套在方法作用域内或代码块内，只对该方法的局部变量是可见的

应用：在一个方法里，需要进行复杂的处理过程，将处理的过程使用一个局部内部类封装

匿名内部类，没有访问修饰符，new关键字实现一个接口或继承一个父类（抽象类），重写接口或父类里面的抽象方法，匿名内部类使用一次就被GC回收了

应用：在BaseAdapter的getView方法中，给每一个item添加一个点击的事件

> [Java的四种内部类](https://www.cnblogs.com/dorothychai/p/6007709.html)

* 谈谈对kotlin的理解

kotlin作为一门新生代的语言，成为当前微信公众号、社区讨论最热门的开发语言之一，有望成为代替Java的Android开发语言。

原因有：

Google主推kotlin作为Android开发的一级语言

IDE支持可以将Java代码一键转成kotlin代码，对Java完美兼容

kotlin编码的大幅度精简、封装大量的函数库、各种语法糖，有效提高了Android的开发效率

> [如何评价 Kotlin 语言？](https://www.zhihu.com/question/25289041)

* 闭包和局部内部类的区别

可以把闭包理解成编程的一种方式，把“包装”在一个类中的类或“包装”在一个方法中的类称为一个闭包

局部内部类是“包装”在一个方法中的类，可以说局部内部类是一个闭包，它的作用是可以在闭包作用域外访问闭包内的方法和属性

按照上述的定义，成员内部类也可以称为一个闭包

```
public interface IEngineer {
    public void work();
}

```

```
public abstract class People {
    public abstract void work();
    public abstract IEngineer getEngineer();
}

```

```
public class Teacher extends People {
    IEngineer engineer;

    @Override
    public void work() {
        System.out.println("I am a teachcer !");

    }

    @Override
    public IEngineer getEngineer() {

        return engineer;
    }

    public void setEngineer(IEngineer engineer) {
        this.engineer = engineer;
    }
}

```

```
    public static void main(String[] args) {
        Teacher teachcer = new Teacher();
        teachcer.setEngineer(new IEngineer() {

            @Override
            public void work() {
                System.out.println("I am a English Teacher");
            }
        });
        teachcer.work();
        //如何访问一个局部内部类的work()方法？
        teachcer.getEngineer().work();
    }

```

> [学习Javascript闭包（Closure）](http://www.ruanyifeng.com/blog/2009/08/learning_javascript_closures.html)
> 
> [什么是闭包？闭包的优缺点？](https://www.cnblogs.com/cxying93/p/6103375.html)
> 
> [为什么说Java匿名内部类是残缺的闭包](https://zhuanlan.zhihu.com/p/29245059)
> 
> [java内部类与内部类的闭包和回调](https://www.jianshu.com/p/367b138fe909)
> 
> [Java语法糖系列五：内部类和闭包](https://www.jianshu.com/p/f55b11a4cec2)

* String 转换成 Integer的方式及原理

**第一种方式：构造函数**

```
Integer integer=new Integer(str);
System.out.println(integer);

```

**第二种方式：parseInt(String,int)**

```
//parseInt(String,int)将一个字符串转换成基本数据类型int
int value=Integer.parseInt(str);
Integer integer=new Integer(value);//装箱操作
System.out.println(integer);

```

**第三种方式：valueOf(String,int)**

```
//valueOf(String,int)将一个字符串转换成对象Integer
Integer integer=Integer.valueOf(str);
System.out.println(integer);

```

*原理分析：*

查看Integer源码，发现将String转换成Integer的过程，先将String转换成基本数据类型int，再通过Integer的**装箱操作**将int转化成对象，所以上述三种方式最终调用下面两个方法:

```
    public Integer(int value) {
        this.value = value;
    }
    public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }

```

将String转换成基本数据类型源码，逐个识别每个字符

```
public static Integer valueOf(String s, int radix) throws NumberFormatException {
        return Integer.valueOf(parseInt(s,radix));
    }

```

```
public static int parseInt(String s, int radix)throws NumberFormatException
    {
    ...

            multmin = limit / radix;
            while (i < len) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                digit = Character.digit(s.charAt(i++),radix);
                if (digit < 0) {
                    throw NumberFormatException.forInputString(s);
                }
                if (result < multmin) {
                    throw NumberFormatException.forInputString(s);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw NumberFormatException.forInputString(s);
                }
                result -= digit;
            }
        } else {
            throw NumberFormatException.forInputString(s);
        }
        return negative ? result : -result;
    }
```

# JavaSE 基础	

有人可能会问对于我们学 Android 的同学来讲，面试还会问 Java 基础吗?答案是会的，但是不会太多，因此我 给了两颗星的重要程度。一般笔试的时候出现 java 基础题的概率比较大，口头面试的时候比较少，比如自己在面试的 时候一些对基础知识比较看重的面试官会深究着 Java 基础去问，比如问你异常的类型以及处理方式，集合的体系结构


## 一、Java 面向对象思想
* 1、面向对象都有哪些特性以及你对这些特性的理解
**继承:**继承是从已有类得到继承信息创建新类的过程。提供继承信息的类被称为父类(超类、基类);得到 继承信息的类被称为子类(派生类)。继承让变化中的软件系统有了一定的延续性，同时继承也是封装程序中可变因 素的重要手段。
**封装:**通常认为封装是把数据和操作数据的方法绑定起来，对数据的访问只能通过已定义的接口。面向对象 的本质就是将现实世界描绘成一系列完全自治、封闭的对象。我们在类中编写的方法就是对实现细节的一种封装;我 们编写一个类就是对数据和数据操作的封装。可以说，封装就是隐藏一切可隐藏的东西，只向外界提供最简单的编程 接口。
**多态性:**多态性是指允许不同子类型的对象对同一消息作出不同的响应。简单的说就是用同样的对象引用调 用同样的方法但是做了不同的事情。多态性分为编译时的多态性和运行时的多态性。如果将对象的方法视为对象向外 界提供的服务，那么运行时的多态性可以解释为:当 A 系统访问 B 系统提供的服务时，B 系统有多种提供服务的方式， 但一切对 A 系统来说都是透明的。方法重载(overload)实现的是编译时的多态性(也称为前绑定)，而方法重写 (override)实现的是运行时的多态性(也称为后绑定)。运行时的多态是面向对象最精髓的东西，要实现多态需要 做两件事:1. 方法重写(子类继承父类并重写父类中已有的或抽象的方法);2. 对象造型(用父类型引用引用子类型 对象，这样同样的引用调用同样的方法就会根据子类对象的不同而表现出不同的行为)。
**抽象:**抽象是将一类对象的共同特征总结出来构造类的过程，包括数据抽象和行为抽象两方面。抽象只关注 对象有哪些属性和行为，并不关注这些行为的细节是什么。

* 2、如何理解 clone 对象(2012/2/24)
* 2.1 为什么要用 clone?
首先，克隆对象是很有必要的，当一个对象需要被多人操作，但是又不想相互影响，需要保持原对象的状态，这 时，就会克隆很多个相同的对象。
* 2.2 new 一个对象的过程和 clone 一个对象的过程区别
new 操作符的本意是分配内存。程序执行到 new 操作符时， 首先去看 new 操作符后面的类型，因为知道了类型， 才能知道要分配多大的内存空间。分配完内存之后，再调用构造函数，填充对象的各个域，这一步叫做对象的初始化， 构造方法返回后，一个对象创建完毕，可以把他的引用(地址)发布到外部，在外部就可以使用这个引用操纵这个对 象。
clone 在第一步是和 new 相似的， 都是分配内存，调用 clone 方法时，分配的内存和原对象(即调用 clone 方 法的对象)相同，然后再使用原对象中对应的各个域，填充新对象的域， 填充完成之后，clone 方法返回，一个新的 相同的对象被创建，同样可以把这个新对象的引用发布到外部。
* 2.3 clone 对象的使用
* 2.3.1 复制对象和复制引用的区别

```
1. Person p = new Person(23, "zhang"); 2. Person p1 = p;
3. System.out.println(p);
4. System.out.println(p1);
```
当 Person p1 = p;执行之后， 是创建了一个新的对象吗? 首先看打印结果:

```
   1.com.itheima.Person@2f9ee1ac 
   2.com.itheima.Person@2f9ee1ac
```

可以看出，打印的地址值是相同的，既然地址都是相同的，那么肯定是同一个对象。p 和 p1 只是引用而已，他们 都指向了一个相同的对象 Person(23, “zhang”) 。 可以把这种现象叫做引用的复制。上面代码执行完成之后， 内 存中的情景如下图所示:

![](/images/15236849613640.jpg)

而下面的代码是真真正正的克隆了一个对象。

```
3.Person p = new Person(23, "zhang"); 
4.Person p1 = (Person) p.clone(); 
5.System.out.println(p); 
6.System.out.println(p1);
```

从打印结果可以看出，两个对象的地址是不同的，也就是说创建了新的对象， 而不是把原对象的地址赋给了一个 新的引用变量:

```
 1. com.itheima.Person@2f9ee1ac 
 2. com.itheima.Person@67f1fba0
```

以上代码执行完成后， 内存中的情景如下图所示:
![](/images/15236850396311.jpg)

2.3.2 深拷贝和浅拷贝
上面的示例代码中，Person 中有两个成员变量，分别是 name 和 age， name 是 String 类型， age 是 int 类型。 代码非常简单，如下所示:

```
public class Person implements Cloneable{
private int age ;
private String name;
public Person(int age, String name) {
this.name = name; 7.    
 }
public Person() {}
public int getAge() {
this.age = age;
}
return age;
}
} }
public String getName() {
    return name;
 protected Object clone() throws CloneNotSupportedException {
    return (Person)super.clone();
}
```
由于 age 是基本数据类型，那么对它的拷贝没有什么疑议，直接将一个 4 字节的整数值拷贝过来就行。但是 name 是 String 类型的， 它只是一个引用， 指向一个真正的 String 对象，那么对它的拷贝有两种方式: 直接将原对象中 的 name 的引用值拷贝给新对象的 name 字段，或者是根据原 Person 对象中的 name 指向的字符串对象创建一个新 的相同的字符串对象，将这个新字符串对象的引用赋给新拷贝的 Person 对象的 name 字段。这两种拷贝方式分别叫 做浅拷贝和深拷贝。深拷贝和浅拷贝的原理如下图所示:
![](/images/15236852293997.jpg)

下面通过代码进行验证。如果两个 Person 对象的 name 的地址值相同， 说明两个对象的 name 都指向同一个 String 对象， 也就是浅拷贝， 而如果两个对象的 name 的地址值不同， 那么就说明指向不同的 String 对象， 也就 是在拷贝 Person 对象的时候， 同时拷贝了 name 引用的 String 对象， 也就是深拷贝。验证代码如下:

```
 Person p = new Person(23, "zhang");
2. Person p1 = (Person) p.clone();
3. String result = p.getName() == p1.getName()
4. ? "clone 是浅拷贝的" : "clone 是深拷贝的";
5. System.out.println(result);
```

```
打印结果为:
6. clone 是浅拷贝的
```

所以，clone 方法执行的是浅拷贝， 在编写程序时要注意这个细节。
如何进行深拷贝:
由上一节的内容可以得出如下结论:如果想要深拷贝一个对象，这个对象必须要实现 Cloneable 接口，实现 clone 方法，并且在 clone 方法内部，把该对象引用的其他对象也要 clone 一份，这就要求这个被引用的对象必须也要实现 Cloneable 接口并且实现 clone 方法。那么，按照上面的结论，实现以下代码 Body 类组合了 Head 类，要想深拷贝 Body 类，必须在 Body 类的 clone 方法中将 Head 类也要拷贝一份。代码如下:


``` java
static class Body implements Cloneable{
2.     public Head head;
3.     public Body() {}
4.     public Body(Head   head) {this.head = head;} 5.     @Override
6.         protected Object   clone() throws CloneNotSupportedException {
                                       7.
8.
9.
10.         }
11.}
12.static class Head implements Cloneable{ 13. public Face face;
Body newBody   = (Body) super.clone(); newBody.head   = (Head) head.clone(); return newBody;
                                         14. public Head() {}
15.         @Override
16. protected Object clone() throws CloneNotSupportedException {
17. return super.clone();
18.   }}

public static void main(String[] args) throws CloneNotSupportedException {
20. Body body = new Body(new Head(new Face()));
21. Body body1 = (Body) body.clone();
22. System.out.println("body == body1 : " + (body == body1) );
23.       System.out.println("body.head == body1.head : " + (body.head == body1.head));
24.}
                  
```      

```
打印结果为:

body == body1 : false
body.head == body1.head : false
```
## 二、Java 中的多态
* 1、Java 中实现多态的机制是什么? 

靠的是父类或接口定义的引用变量可以指向子类或具体实现类的实例对象，而程序调用的方法在运行期才动 态绑定，就是引用变量所指向的具体实例对象的方法，也就是内存里正在运行的那个对象的方法，而不是引用变 量的类型中定义的方法。
## 三、Java 的异常处理
* 1、Java 中异常分为哪些种类

1)按照异常需要处理的时机分为编译时异常也叫 CheckedException 和运行时异常也叫 RuntimeException。只有 java 语言提供了 Checked 异常，Java 认为 Checked 异常都是可以被处理的异常， 所以 Java 程序必须显式处理 Checked 异常。如果程序没有处理 Checked 异常，该程序在编译时就会发生错 误无法编译。这体现了 Java 的设计哲学:没有完善错误处理的代码根本没有机会被执行。对 Checked 异常 处理方法有两种:

* 1 当前方法知道如何处理该异常，则用 try...catch 块来处理该异常。 
* 2 当前方法不知道如何处理，则在定义该方法是声明抛出该异常。



