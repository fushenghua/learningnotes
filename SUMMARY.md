# Summary

* [README](./README.md)
* [Android](Android/0-README.md)
  * [Activity启动过程全解析](Android/Activity启动过程全解析.md)
  * [AIDL](Android/AIDL.md)
  * [Android关于oom的解决方案](Android/Android关于oom的解决方案.md)
  * [Android内存泄漏总结](Android/Android内存泄漏总结.md)
  * [Android几种进程](Android/Android几种进程.md)
  * [Android图片中的三级缓存](Android/Android图片中的三级缓存.md)
  * [Android基础知识](Android/Android基础知识.md)
  * [Android开机过程](Android/Android开机过程.md)
  * [Android性能优化](Android/Android性能优化.md)
  * [Android系统机制](Android/Android系统机制.md)
  * [ANR问题](Android/ANR问题.md)
  * [Art和Dalvik区别](Android/Art和Dalvik区别.md)
  * [Asynctask源码分析](Android/Asynctask源码分析.md)
  * [Binder机制](Android/Binder机制.md)
  * [Bitmap的分析与使用](Android/Bitmap的分析与使用.md)
  * [EventBus用法详解](Android/EventBus用法详解.md)
  * [Git操作](Android/Git操作.md)
  * [Handler内存泄漏分析及解决](Android/Handler内存泄漏分析及解决.md)
  * [Listview详解](Android/Listview详解.md)
  * [MVC,MVP,MVVM的区别](Android/MVC,MVP,MVVM的区别.md)
  * [Recyclerview和Listview的异同](Android/Recyclerview和Listview的异同.md)
  * [SurfaceView](Android/SurfaceView.md)
  * [Zygote和System进程的启动过程](Android/Zygote和System进程的启动过程.md)
  * [事件分发机制](Android/事件分发机制.md)
  * [开源框架源码分析](Android/开源框架源码分析.md)
  * [插件化技术学习](Android/插件化技术学习.md)
  * [查漏补缺](Android/查漏补缺.md)
  * [热修复技术](Android/热修复技术.md)
* [Android-Basis](Android-Basis/0-README.md)
  * [Activity全方位解析](Android-Basis/Activity全方位解析.md)
  * [Android Bitmap压缩策略](Android-Basis/Android Bitmap压缩策略.md)
  * [Android Context详解](Android-Basis/Android Context详解.md)
  * [Android事件分发机制](Android-Basis/Android事件分发机制.md)
  * [Android动画总结](Android-Basis/Android动画总结.md)
  * [Android消息机制](Android-Basis/Android消息机制.md)
  * [Android虚拟机及编译过程](Android-Basis/Android虚拟机及编译过程.md)
  * [Android进程优先级](Android-Basis/Android进程优先级.md)
  * [Android进程间通信方式](Android-Basis/Android进程间通信方式.md)
  * [AsyncTask详解](Android-Basis/AsyncTask详解.md)
  * [BroadcastReceiver全方位解析](Android-Basis/BroadcastReceiver全方位解析.md)
  * [ContentProvider全方位解析](Android-Basis/ContentProvider全方位解析.md)
  * [Fragment详解](Android-Basis/Fragment详解.md)
  * [HandlerThread详解](Android-Basis/HandlerThread详解.md)
  * [IntentService详解](Android-Basis/IntentService详解.md)
  * [LruCache原理解析](Android-Basis/LruCache原理解析.md)
  * [Service全方位解析](Android-Basis/Service全方位解析.md)
  * [View测量、布局及绘制原理](Android-Basis/View测量、布局及绘制原理.md)
  * [Window、Activity、DecorView以及ViewRoot之间的关系](Android-Basis/Window、Activity、DecorView以及ViewRoot之间的关系.md)
* [Android-Part2](Android-Part2/0-README.md)
  * [activity-window-view三者的差别-fragment的特点-360](Android-Part2/activity-window-view三者的差别-fragment的特点-360.md)
  * [AndroidService与Activity之间通信的几种方式](Android-Part2/AndroidService与Activity之间通信的几种方式.md)
  * [Android为每个应用程序分配的内存大小是多少？-美团](Android-Part2/Android为每个应用程序分配的内存大小是多少？-美团.md)
  * [Android代码中实现WAP方式联网-360](Android-Part2/Android代码中实现WAP方式联网-360.md)
  * [Android动画框架实现原理](Android-Part2/Android动画框架实现原理.md)
  * [Android各个版本API的区别](Android-Part2/Android各个版本API的区别.md)
  * [Android属性动画特性-乐视-小米](Android-Part2/Android属性动画特性-乐视-小米.md)
  * [Android网络编程面试题集](Android-Part2/Android网络编程面试题集.md)
  * [Android面试题集](Android-Part2/Android面试题集.md)
  * [ContentProvider-乐视](Android-Part2/ContentProvider-乐视.md)
  * [fragment生命周期](Android-Part2/fragment生命周期.md)
  * [Glide源码解析](Android-Part2/Glide源码解析.md)
  * [Handler、Thread和HandlerThread的差别-小米](Android-Part2/Handler、Thread和HandlerThread的差别-小米.md)
  * [intentService作用是什么，AIDL解决了什么问题？-小米](Android-Part2/intentService作用是什么，AIDL解决了什么问题？-小米.md)
  * [invalidate和postInvalidate的区别及使用-百度](Android-Part2/invalidate和postInvalidate的区别及使用-百度.md)
  * [launchmode应用场景-百度-小米-乐视](Android-Part2/launchmode应用场景-百度-小米-乐视.md)
  * [LinearLayout对比RelativeLayout-百度](Android-Part2/LinearLayout对比RelativeLayout-百度.md)
  * [Requestlayout，onlayout，onDraw，DrawChild区别与联系-猎豹](Android-Part2/Requestlayout，onlayout，onDraw，DrawChild区别与联系-猎豹.md)
  * [Touch事件传递流程-小米](Android-Part2/Touch事件传递流程-小米.md)
  * [Ubuntu编译安卓系统-百度](Android-Part2/Ubuntu编译安卓系统-百度.md)
  * [view绘制流程-百度](Android-Part2/view绘制流程-百度.md)
  * [volley解析-美团-乐视](Android-Part2/volley解析-美团-乐视.md)
  * [什么情况导致oom-乐视-美团](Android-Part2/什么情况导致oom-乐视-美团.md)
  * [什么情况导致内存泄漏-美团](Android-Part2/什么情况导致内存泄漏-美团.md)
  * [优化自定义view百度-乐视-小米](Android-Part2/优化自定义view百度-乐视-小米.md)
  * [低版本SDK实现高版本api-小米](Android-Part2/低版本SDK实现高版本api-小米.md)
  * [多线程-360](Android-Part2/多线程-360.md)
  * [如何保证service在后台不被kill](Android-Part2/如何保证service在后台不被kill.md)
  * [如何导入外部数据库](Android-Part2/如何导入外部数据库.md)
  * [描述一次网络请求的流程-新浪](Android-Part2/描述一次网络请求的流程-新浪.md)
  * [本地广播和全局广播有什么差别](Android-Part2/本地广播和全局广播有什么差别.md)
  * [架构设计-搜狐](Android-Part2/架构设计-搜狐.md)
  * [线程同步-百度](Android-Part2/线程同步-百度.md)
* [Android-Part3](Android-Part3/0-README.md)
  * [Android Apk安装过程](Android-Part3/Android Apk安装过程.md)
  * [Android Binder机制及AIDL使用](Android-Part3/Android Binder机制及AIDL使用.md)
  * [Android MVP模式详解](Android-Part3/Android MVP模式详解.md)
  * [Android Parcelable和Serializable的区别](Android-Part3/Android Parcelable和Serializable的区别.md)
  * [Android 内存泄漏总结](Android-Part3/Android 内存泄漏总结.md)
  * [Android全局异常处理](Android-Part3/Android全局异常处理.md)
  * [Android基础\高端技术面试题](Android-Part3/Android基础\高端技术面试题.md)
  * [Android多线程断点续传](Android-Part3/Android多线程断点续传.md)
  * [Android布局优化之include、merge、ViewStub的使用](Android-Part3/Android布局优化之include、merge、ViewStub的使用.md)
  * [Android性能优化总结](Android-Part3/Android性能优化总结.md)
  * [Android推送技术解析](Android-Part3/Android推送技术解析.md)
  * [Android插件化入门指南](Android-Part3/Android插件化入门指南.md)
  * [Android权限处理](Android-Part3/Android权限处理.md)
  * [Android热修复原理](Android-Part3/Android热修复原理.md)
  * [PopupWindow和Dialog区别](Android-Part3/PopupWindow和Dialog区别.md)
  * [VirtualApk解析](Android-Part3/VirtualApk解析.md)
  * [一个APP从启动到主页面显示经历了哪些过程？](Android-Part3/一个APP从启动到主页面显示经历了哪些过程？.md)
* [Android-source](Android-source/0-README.md)
  * [ Bitmap的深入理解](Android-source/ Bitmap的深入理解.md)
  * [Android Handler与Looper原理浅析](Android-source/Android Handler与Looper原理浅析.md)
  * [EventBus解析](Android-source/EventBus解析.md)
  * [OkHttp解析](Android-source/OkHttp解析.md)
  * [Retrofit解析](Android-source/Retrofit解析.md)
  * [ViewGroup源码解析](Android-source/ViewGroup源码解析.md)
  * [深入Android渲染机制](Android-source/深入Android渲染机制.md)
  * [谈谈对Okhttp的理解](Android-source/谈谈对Okhttp的理解.md)
* [DataStructure-程序员代码面试指南(左程云)](DataStructure-程序员代码面试指南(左程云)/0-README.md)
  * [1.设计一个有getMin功能的栈](DataStructure-程序员代码面试指南(左程云)/1.设计一个有getMin功能的栈.md)
  * [2.由两个栈组成的队列](DataStructure-程序员代码面试指南(左程云)/2.由两个栈组成的队列.md)
  * [3.如何仅用递归函数和栈操作逆序一个栈](DataStructure-程序员代码面试指南(左程云)/3.如何仅用递归函数和栈操作逆序一个栈.md)
  * [冒泡排序](DataStructure-程序员代码面试指南(左程云)/冒泡排序.md)
  * [归并排序](DataStructure-程序员代码面试指南(左程云)/归并排序.md)
  * [快速排序](DataStructure-程序员代码面试指南(左程云)/快速排序.md)
  * [折半查找](DataStructure-程序员代码面试指南(左程云)/折半查找.md)
  * [数据结构(Java)](DataStructure-程序员代码面试指南(左程云)/数据结构(Java).md)
  * [数据结构与算法面试题集](DataStructure-程序员代码面试指南(左程云)/数据结构与算法面试题集.md)
  * [数组](DataStructure-程序员代码面试指南(左程云)/数组.md)
  * [栈和队列](DataStructure-程序员代码面试指南(左程云)/栈和队列.md)
  * [选择排序](DataStructure-程序员代码面试指南(左程云)/选择排序.md)
  * [递归和非递归方式实现二叉树先、中、后序遍历](DataStructure-程序员代码面试指南(左程云)/递归和非递归方式实现二叉树先、中、后序遍历.md)
  * [面试中的 10 大排序算法总结](DataStructure-程序员代码面试指南(左程云)/面试中的 10 大排序算法总结.md)
  * [顺序查找](DataStructure-程序员代码面试指南(左程云)/顺序查找.md)
* [DesignPattern](DesignPattern/0-README.md)
  * [Builder模式](DesignPattern/Builder模式.md)
  * [代理模式](DesignPattern/代理模式.md)
  * [单例模式](DesignPattern/单例模式.md)
  * [原型模式](DesignPattern/原型模式.md)
  * [外观模式](DesignPattern/外观模式.md)
  * [常见的面向对象设计原则](DesignPattern/常见的面向对象设计原则.md)
  * [策略模式](DesignPattern/策略模式.md)
  * [简单工厂](DesignPattern/简单工厂.md)
  * [观察者模式](DesignPattern/观察者模式.md)
  * [责任链模式](DesignPattern/责任链模式.md)
  * [适配器模式](DesignPattern/适配器模式.md)
* [Java](Java/0-README.md)
  * [Java基础知识](Java/Java基础知识.md)
  * [Java常用数据结构](Java/Java常用数据结构.md)
  * [Java源码深入学习](Java/Java源码深入学习.md)
  * [Java面试题集](Java/Java面试题集.md)
* [Java-Part2](Java-Part2/0-README.md)
  * [ArrayList 、 LinkedList 、 Vector 的底层实现和区别](Java-Part2/ArrayList 、 LinkedList 、 Vector 的底层实现和区别.md)
  * [Arraylist](Java-Part2/Arraylist.md)
  * [Arraylist和Hashmap如何扩容等](Java-Part2/Arraylist和Hashmap如何扩容等.md)
  * [ArrayList源码剖析](Java-Part2/ArrayList源码剖析.md)
  * [Collection](Java-Part2/Collection.md)
  * [hashmap和hashtable的底层实现和区别，两者和concurrenthashmap的区别。](Java-Part2/hashmap和hashtable的底层实现和区别，两者和concurrenthashmap的区别。.md)
  * [HashMap源码剖析](Java-Part2/HashMap源码剖析.md)
  * [Hashmap的hashcode的作用等](Java-Part2/Hashmap的hashcode的作用等.md)
  * [HashTable源码剖析](Java-Part2/HashTable源码剖析.md)
  * [Java中的内存泄漏](Java-Part2/Java中的内存泄漏.md)
  * [Java基础知识](Java-Part2/Java基础知识.md)
  * [Java集合框架](Java-Part2/Java集合框架.md)
  * [LinkedHashMap源码剖析](Java-Part2/LinkedHashMap源码剖析.md)
  * [Linkedlist](Java-Part2/Linkedlist.md)
  * [LinkedList源码剖析](Java-Part2/LinkedList源码剖析.md)
  * [List](Java-Part2/List.md)
  * [Queue](Java-Part2/Queue.md)
  * [Set](Java-Part2/Set.md)
  * [String源码分析](Java-Part2/String源码分析.md)
  * [Vector源码剖析](Java-Part2/Vector源码剖析.md)
  * [从源码分析HashMap](Java-Part2/从源码分析HashMap.md)
  * [反射机制](Java-Part2/反射机制.md)
  * [如何表达出Collection及其子类](Java-Part2/如何表达出Collection及其子类.md)
* [Java-Part3](Java-Part3/0-README.md)
  * [Java内存区域与内存溢出](Java-Part3/Java内存区域与内存溢出.md)
  * [JVM](Java-Part3/JVM.md)
  * [JVM类加载机制](Java-Part3/JVM类加载机制.md)
  * [NIO](Java-Part3/NIO.md)
  * [垃圾回收算法](Java-Part3/垃圾回收算法.md)
* [Java-Thread](Java-Thread/0-README.md)
  * [Java并发基础知识](Java-Thread/Java并发基础知识.md)
  * [Java线程、多线程和线程池](Java-Thread/Java线程、多线程和线程池.md)
  * [Synchronized](Java-Thread/Synchronized.md)
  * [Thread和Runnable实现多线程的区别](Java-Thread/Thread和Runnable实现多线程的区别.md)
  * [volatile变量修饰符](Java-Thread/volatile变量修饰符.md)
  * [使用wait notify notifyall实现线程间通信](Java-Thread/使用wait notify notifyall实现线程间通信.md)
  * [可重入内置锁](Java-Thread/可重入内置锁.md)
  * [多线程环境中安全使用集合API](Java-Thread/多线程环境中安全使用集合API.md)
  * [守护线程与阻塞线程](Java-Thread/守护线程与阻塞线程.md)
  * [实现内存可见的两种方法比较：加锁和volatile变量](Java-Thread/实现内存可见的两种方法比较：加锁和volatile变量.md)
  * [死锁](Java-Thread/死锁.md)
  * [生产者和消费者问题](Java-Thread/生产者和消费者问题.md)
  * [线程中断](Java-Thread/线程中断.md)
* [Network](Network/0-README.md)
  * [Android网络编程面试题集](Network/Android网络编程面试题集.md)
  * [Http协议](Network/Http协议.md)
  * [Socket](Network/Socket.md)
  * [TCP与UDP](Network/TCP与UDP.md)
  * [计算机网络基础汇总](Network/计算机网络基础汇总.md)
* [OperatingSystem](OperatingSystem/0-README.md)
  * [Linux系统的IPC](OperatingSystem/Linux系统的IPC.md)
  * [操作系统](OperatingSystem/操作系统.md)
* [ReadingNotes](ReadingNotes/0-README.md)
  * [Android开发艺术探索总结](ReadingNotes/Android开发艺术探索总结.md)
  * [《Android开发艺术探索》第一章笔记](ReadingNotes/《Android开发艺术探索》第一章笔记.md)
  * [《Android开发艺术探索》第三章笔记](ReadingNotes/《Android开发艺术探索》第三章笔记.md)
  * [《Android开发艺术探索》第二章笔记](ReadingNotes/《Android开发艺术探索》第二章笔记.md)
  * [《Android开发艺术探索》第八章笔记](ReadingNotes/《Android开发艺术探索》第八章笔记.md)
  * [《Android开发艺术探索》第十五章笔记](ReadingNotes/《Android开发艺术探索》第十五章笔记.md)
  * [《Android开发艺术探索》第四章笔记](ReadingNotes/《Android开发艺术探索》第四章笔记.md)
  * [《APP研发录》第1章读书笔记](ReadingNotes/《APP研发录》第1章读书笔记.md)
  * [《APP研发录》第2章读书笔记](ReadingNotes/《APP研发录》第2章读书笔记.md)
  * [《Java编程思想》第一章读书笔记](ReadingNotes/《Java编程思想》第一章读书笔记.md)
  * [《Java编程思想》第二章读书笔记](ReadingNotes/《Java编程思想》第二章读书笔记.md)
  * [《深入理解java虚拟机》第12章](ReadingNotes/《深入理解java虚拟机》第12章.md)
* [subject-Interview](subject-Interview/0-README.md)
  * [BAT-interview](subject-Interview/BAT-interview.md)
  * [性能优化\图片](subject-Interview/性能优化\图片.md)
  * [插件化](subject-Interview/插件化.md)
  * [数据库](subject-Interview/数据库.md)
  * [网易杭研](subject-Interview/网易杭研.md)
  * [蜻蜓FM](subject-Interview/蜻蜓FM.md)
  * [豌豆荚](subject-Interview/豌豆荚.md)
* [subject-Java](subject-Java/0-README.md)
  * [==和equals和hashCode的区别-乐视](subject-Java/==和equals和hashCode的区别-乐视.md)
  * [ArrayMap对比HashMap](subject-Java/ArrayMap对比HashMap.md)
  * [hashmap和hashtable的区别-乐视-小米](subject-Java/hashmap和hashtable的区别-乐视-小米.md)
  * [HashMap的实现原理-美团](subject-Java/HashMap的实现原理-美团.md)
  * [int-char-long各占多少字节数](subject-Java/int-char-long各占多少字节数.md)
  * [int与integer的区别](subject-Java/int与integer的区别.md)
  * [java多态-乐视](subject-Java/java多态-乐视.md)
  * [java排序查找算法-美团](subject-Java/java排序查找算法-美团.md)
  * [java状态机](subject-Java/java状态机.md)
  * [java虚拟机的特性-百度-乐视](subject-Java/java虚拟机的特性-百度-乐视.md)
  * [string-stringbuffer-stringbuilder区别-小米-乐视-百度](subject-Java/string-stringbuffer-stringbuilder区别-小米-乐视-百度.md)
  * [什么导致线程阻塞-58-美团](subject-Java/什么导致线程阻塞-58-美团.md)
  * [内部类](subject-Java/内部类.md)
  * [内部类的作用-乐视](subject-Java/内部类的作用-乐视.md)
  * [列举java的集合和继承关系-百度-美团](subject-Java/列举java的集合和继承关系-百度-美团.md)
  * [双亲委派模型-滴滴](subject-Java/双亲委派模型-滴滴.md)
  * [哪些情况下的对象会被垃圾回收机制处理掉-美团-小米](subject-Java/哪些情况下的对象会被垃圾回收机制处理掉-美团-小米.md)
  * [容器类之间的区别-乐视-美团](subject-Java/容器类之间的区别-乐视-美团.md)
  * [并发编程-猎豹](subject-Java/并发编程-猎豹.md)
  * [抽象类接口区别-360](subject-Java/抽象类接口区别-360.md)
  * [抽象类的意义-乐视](subject-Java/抽象类的意义-乐视.md)
  * [接口的意义-百度](subject-Java/接口的意义-百度.md)
  * [泛型中extends和super的区别](subject-Java/泛型中extends和super的区别.md)
  * [父类的静态方法能否被子类重写-猎豹](subject-Java/父类的静态方法能否被子类重写-猎豹.md)
  * [进程和线程的区别-猎豹-美团](subject-Java/进程和线程的区别-猎豹-美团.md)
* [subject-LeetCode](subject-LeetCode/0-README.md)
  * [1.七种方式实现singleton模式](subject-LeetCode/1.七种方式实现singleton模式.md)
  * [2.二维数组中的查找](subject-LeetCode/2.二维数组中的查找.md)
  * [two-sum](subject-LeetCode/two-sum.md)
  * [zigzag-conversion](subject-LeetCode/zigzag-conversion.md)
  * [合并两个排序的链表](subject-LeetCode/合并两个排序的链表.md)
  * [旋转数组的最小数字](subject-LeetCode/旋转数组的最小数字.md)
  * [面试题11：数值的整数次方](subject-LeetCode/面试题11：数值的整数次方.md)
  * [面试题12：打印1到最大的n位数](subject-LeetCode/面试题12：打印1到最大的n位数.md)
  * [面试题44：扑克牌的顺子](subject-LeetCode/面试题44：扑克牌的顺子.md)
  * [面试题45：圆圈中最后剩下的数字](subject-LeetCode/面试题45：圆圈中最后剩下的数字.md)
  * [面试题6：重建二叉树](subject-LeetCode/面试题6：重建二叉树.md)
